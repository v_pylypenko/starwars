import React from 'react';
import {SwapiServiceConcumer} from "../swapi-service-context";

const withSwapiService =(mapMethodToProps) => (Wrapped) =>{
  return(props) =>{
     return (
         <SwapiServiceConcumer>
             {
                 (swapiService) => {
                     const serviceProps = mapMethodToProps(swapiService);
                     return(
                         <Wrapped {...props} {...serviceProps}/>
                     )
                 }
             }
         </SwapiServiceConcumer>
     )
  }
};

export default withSwapiService;