import React, {Component} from 'react';
import Header from '../header';
import RandomPlanet from '../random-planet';
import ErrorBoundry from "../error-boundry";
import SwapiServise from '../../services/swapi-service';
import DummySwapiService from "../../services/dummy-swapi-service";
import {SwapiServiceProvider} from '../swapi-service-context';
import {
    PeoplePage,
    PlanetsPage,
    StarShipsPage,
    LoginPage,
    SecretPage
} from "../pages";

import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import StarshipDetails from "../sw-components/starship-details";

export default class App extends Component{

    state = {
        hasError: false,
        swapiService: new SwapiServise(),
        isLoggedIn: false
    };

    onLogin = ()=>{
        this.setState({isLoggedIn: true})
    };

    onServiceChange = () =>{
      this.setState(({swapiService}) =>{
          const Service = swapiService instanceof SwapiServise ? DummySwapiService : SwapiServise;
          return{
              swapiService: new Service()
          };
      })
    };

    componentDidCatch(error, errorInfo) {
        console.log('componentDidCatch()');
        this.setState({hasError: true})
    }

    render() {
        const {isLoggedIn} = this.state;
        return(
            <ErrorBoundry>
                <SwapiServiceProvider value={this.state.swapiService}>
                    <Router>
                        <div className="container">
                            <Header onServiceChange={this.onServiceChange}/>
                            <RandomPlanet/>
                            <Switch>
                                <Route path="/" render={()=> <h2>Welcome to my pet project</h2>} exact/>
                                <Route path="/people/:id?" component={PeoplePage}/>
                                <Route path="/planets" component={PlanetsPage}/>
                                <Route path="/starships" component={StarShipsPage} exact/>
                                <Route
                                    path="/starships/:id"
                                    render={({match})=>{
                                        const {id} = match.params;
                                        return <StarshipDetails itemId={id}/>}}
                                />
                                <Route path="/login" render={()=> <LoginPage isLoggedIn={isLoggedIn} onLogin={this.onLogin}/>}/>
                                <Route path="/secret" render={()=> <SecretPage isLoggedIn={isLoggedIn}/>}/>
                                <Redirect to="/"/>
                            </Switch>
                        </div>
                    </Router>
                </SwapiServiceProvider>
            </ErrorBoundry>
        )
    }
};

