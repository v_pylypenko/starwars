import React, { Component } from 'react';
import SwapiService from '../../services/swapi-service'
import './random-planet.css';
import Spinner from "../spinner";
import ErrorIndicate from '../err-indicator';
import PropTypes from 'prop-types';

export default class RandomPlanet extends Component {

    swapiService = new SwapiService();

    static defaultProps = {
        updateInterval: 10000
    };
    static propTypes={
        updateInterval: PropTypes.number
    };
    state = {
        planet: {},
        loading: true,
        err: false
    };

    componentDidMount() {
        this.updatePlanet();
        this.interval = setInterval(this.updatePlanet, this.props.updateInterval);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    onPlanetLoaded = (planet) => {
        this.setState({
            planet,
            loading: false,
            err: false
        })
    };

    onError = (err) => {
        this.setState({
            err: true,
            loading: false
        })
    };

    updatePlanet = () => {
        const id = Math.floor(Math.random() * 25) + 2;

        this.swapiService.getPlanet(id)
            .then(this.onPlanetLoaded)
            .catch(this.onError);
    };

    render() {
        const {planet, loading, err} = this.state;

        const hasData = !(loading || err);
        const errMessage = err ? <ErrorIndicate/> : null;
        const spinner = loading ? <Spinner/> : null;
        const content = hasData ? <PlanetView planet={planet}/> : null;

        return (
            <div className="container jumbotron random-planet ">
                {errMessage}
                {spinner}
                {content}
            </div>

        );
    }
}

const PlanetView = ({planet}) => {
    const {id, name, population, rotPeriod, diameter} = planet;

    return(
      <React.Fragment>
          <img className="planet-image"
               src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`}
          alt="img"/>
          <div>
              <h4>{name}</h4>
              <ul className="list-group list-group-flush">
                  <li className="list-group-item">
                      <span className="term">Population</span>
                      <span>{population}</span>
                  </li>
                  <li className="list-group-item">
                      <span className="term">Rotation Period</span>
                      <span>{rotPeriod}</span>
                  </li>
                  <li className="list-group-item">
                      <span className="term">Diameter</span>
                      <span>{diameter}</span>
                  </li>
              </ul>
          </div>
      </React.Fragment>
    )
};