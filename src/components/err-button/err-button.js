import React, { Component } from 'react';

export default class ErrButton extends Component{
    state={
        renderErr:false
    };

    render(){
        if (this.state.renderErr)
            this.foo.bar = 0;
        return(
            <button className="btn btn-danger btn-lg"
            onClick={() => this.setState({renderErr:true})}>
            Throw Error
            </button>
        );
    }

}