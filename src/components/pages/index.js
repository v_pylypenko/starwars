import PeoplePage from './people-page'
import PlanetsPage from './planets-page'
import StarShipsPage from './starships-page'
import LoginPage from './login-page'
import SecretPage from './secret-page'

export{
    PeoplePage,
    PlanetsPage,
    StarShipsPage,
    LoginPage,
    SecretPage
};