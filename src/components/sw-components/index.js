import{
    PeopleList,
    StarshipList,
    PlanetList
} from './item-lists';

import PersonDetails from './person-details';

import StarshipDetails from './starship-details';

import PlanetDetails from './planet-details';

export {
    PeopleList,
    StarshipList,
    PlanetList,
    PersonDetails,
    StarshipDetails,
    PlanetDetails
};