import React from 'react'
import {withData, withSwapiService} from '../hoc-helpers';
import ItemList from '../item-list';
import {withChildFunction, compose} from "../hoc-helpers";


const mapStarshipsProps = (swapiService) =>{
    return{
        getData: swapiService.getAllStarships
    }
};

const mapPeopleProps = (swapiService) =>{
    return{
        getData: swapiService.getAllPeople
    }
};

const mapPlanetsProps = (swapiService) =>{
    return{
        getData: swapiService.getAllPlanets
    }
};

const renderName = ({name}) => <span>{name}</span>;

const renderModel = ({name, model}) => <span>{name}({model})</span>;

const PeopleList = compose(
    withSwapiService(mapPeopleProps),
    withData,
    withChildFunction(renderName)
    )(ItemList);

const StarshipList = compose(
    withSwapiService(mapStarshipsProps),
    withData,
    withChildFunction(renderModel)
)(ItemList);

const PlanetList = compose(
    withSwapiService(mapPlanetsProps),
    withData,
    withChildFunction(renderName)
)(ItemList);

export{
    PeopleList,
    StarshipList,
    PlanetList
};