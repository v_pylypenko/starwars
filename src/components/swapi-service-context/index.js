import {
    SwapiServiceProvider,
    SwapiServiceConcumer
} from './swapi-service-context'

export {
    SwapiServiceProvider,
    SwapiServiceConcumer
};